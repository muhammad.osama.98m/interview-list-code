#include "../include/list.h"
#include "../src/list.c"
#include "../src/xorshift.c"
#include <stdio.h>

void
PrintList( const List L )
{
    Position P = Header( L );

    if( IsEmpty( L ) )
        printf( "Empty list\n" );
    else
    {
        do
        {
            P = Advance( P );
            printf( "%d ->", Retrieve( P ) );
        } while( !IsLast( P, L ) );
        printf("...\n");
    }
}

Position
FindMidPt( const List L)
{
    Position slw_P = Header( L );
    Position fst_P = Header( L );
    if( IsEmpty( L ) )
        printf( "Empty List\n" );
    else{
        while( !IsLast( fst_P, L)){
           slw_P = Advance(slw_P);
           fst_P = Skip_By_One(fst_P, L);
        }
    }
    
    return slw_P;
}

int main( )
{
    List L;
    Position P;
    int i;

    L = MakeEmpty( NULL );
    P = Header( L );

    for( i = 0; i < 11; i++ )
    {
        Insert( xorshift128()%64, L, P );
        P = Advance( P );
    }
    
    PrintList( L );

    printf("Mid Point of List is %d\n", FindMidPt( L )->Element);

    DeleteList( L );

    return 0;
}
