#include <stdio.h>
#include <stdint.h>

uint32_t w = 3, x = 1, y = 5, z = 8;	//seed state variables, initialize to non-zero value.

uint32_t xorshift128(void){
		
		uint32_t t = x;
		t ^= t << 11U;
		t ^= t >> 8U;
		x = y; y = z; z = w;
		w ^= w >> 19U;
		w ^= t;
		
		return w;
		
	}
